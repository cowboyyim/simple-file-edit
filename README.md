### Prerequisite

- Backend: Flask
- Frontend: React

### Quick Overview

```
pip install Flask
FLASK_APP=index.py flask run
```

### API

- GET /api/get_file : return file content (example: abc.txt)
- POST /api/update_file: create or update file content
