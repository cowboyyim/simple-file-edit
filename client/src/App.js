import React, { Component } from "react";
import "bulma/css/bulma.css";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  state = {
    buttonText: "Edit",
    isEdit: false,
    isLoaded: false,
    error: "",
    fileContent: ""
  };

  componentDidMount() {
    this.getFile();
  }

  getFile() {
    fetch("/api/get_file")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            fileContent: result.detail
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  saveFile() {
    fetch("/api/update_file", {
      body: JSON.stringify({ content: this.state.fileContent }),
      headers: { "content-type": "application/json" },
      method: "POST"
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          console.log(error);
        }
      );
  }

  handleChange(event) {
    this.setState({ fileContent: event.target.value });
  }

  saveOrEdit() {
    const editing = !this.state.isEdit;
    this.setState({ isEdit: editing });
    if (editing) {
      this.setState({ buttonText: "Save" });
    } else {
      this.saveFile();
      this.setState({ buttonText: "Edit" });
    }
  }

  render() {
    return (
      <div className="App">
        <section className="section">
          <div className="container">
            <button
              className="button is-link"
              onClick={this.saveOrEdit.bind(this)}
            >
              {this.state.buttonText}
            </button>
            <div className="content">
              <textarea
                className="textarea"
                placeholder="10 lines of textarea"
                rows="10"
                value={this.state.fileContent}
                readOnly={!this.state.isEdit}
                onChange={this.handleChange.bind(this)}
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;
