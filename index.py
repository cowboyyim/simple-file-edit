from flask import Flask, jsonify, request, abort, render_template, redirect, send_from_directory
from pathlib import Path

app = Flask(__name__)
file_path = Path("./abc.txt")

# @app.route('/static/<path:path>')
# def send_static_files(path):
#   return send_from_directory(('template','static'), path)

@app.route("/", methods=['GET', 'POST'])
def index():
  return render_template("index.html") 

@app.route("/api/get_file")
def get_file():
  if file_path.is_file():
    with open(file_path, "r") as file:
      detail = file.read()
      return jsonify({"detail": detail}), 201
  else:
    abort(404)

@app.route("/api/update_file", methods=['POST'])
def update_file():
  json_content = request.get_json()
  content = json_content.get('content')
  with open(file_path, "w+") as file:
    file.write(content)
    return jsonify({"success": True}), 201

if __name__ == '__main__':
  app.run(debug=True)